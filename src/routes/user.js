/**
 * @author Paulo Silva
 * @description Routes User
 * @returns methods <post>
 * @version 0.0.1
 */

/**
 * @requires express
 */
const express = require('express');

/**
 * @requires express.Router
 */
const router = express.Router();

/**
 * @description User Controller
 */
const userController = require('../users/user.controller');

// define route to authenticate user
router.post('/auth', (req, res) => {
  userController.authenticate(req.body).then((response) => {
    response ? res.json(response) : res.status(400).json({ message: 'Username or password is incorrect' });
  });
});
// define route to register user
router.post('/register', (req, res) => {
  userController.register(req.body).then((response) => {
    res.json(response);
  });
});

module.exports = router;
