/**
* @author Paulo Silva
* @description Instance MongoDB with Mongoose, Build and Validate Schema
* @version 0.0.1
*/

/**
* @requires mongoose
*/
const mongoose = require('mongoose');

// Import user model
const User = require('../users/user.model');

/**
* @description connect mongo db
*/
mongoose.connect(`mongodb://${process.env.URI_DATA}`, { useNewUrlParser: true });

module.exports = {
  User,
};
