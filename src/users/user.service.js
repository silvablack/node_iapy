/**
* @author Paulo Silva
* @description User service to handler of model
* @version 0.0.1
*/

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/config.json');
const db = require('../config/db.js');

async function authenticate({ username, password }) {
  const user = await db.User.findOne({ username });
  if (user && bcrypt.compareSync(password, user.password)) {
    const userObject = user.toObject();
    const token = jwt.sign({ sub: user.id }, config.secret);
    return {
      userObject,
      token,
    };
  }
  return Promise.resolve();
}

async function getAll() {
  return db.User.find().select('-password');
}

async function getById(id) {
  return db.User.findById(id).select('-password');
}

async function create(userParam) {
  if (await db.User.findOne({ username: userParam.username })) {
    throw new Error(`Username ${userParam.username} is already taken`);
  }

  const user = new db.User(userParam);
  if (userParam.password) {
    user.password = bcrypt.hashSync(userParam.password, 10);
  }

  await user.save();
  return user;
}

async function update(id, userParam) {
  const user = await db.User.findById(id);

  userParam.hash = false;

  if (!user) throw new Error('User not found');

  if (user.username !== userParam.username
      && await db.User.findOne({ username: userParam.username })) {
    throw new Error(`Username${userParam.username}is already taken`);
  }

  if (userParam.password) {
    userParam.hash = bcrypt.hashSync(userParam.password, 10);
  }

  Object.assign(user, userParam);
  await user.save();
}

async function deleteOne(id) {
  await db.User.findByIdAndRemove(id);
}

module.exports = {
  authenticate,
  getAll,
  getById,
  create,
  update,
  delete: deleteOne,
};
