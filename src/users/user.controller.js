/**
* @author Paulo Silva
* @description User controller to request service and get and return response
* @version 0.0.1
*/

const userService = require('./user.service');

/**
 * @description Authenticate controller, the function call userService.authenticate
 * @param {username, password} data
 */
function authenticate(data) {
  return userService.authenticate(data)
    .then(user => user).catch(err => err);
}

/**
 * @description Register user controller, the function call userService.create
 * @param {username, password, firstName, lastName} data
 */
function register(data) {
  return userService.create(data)
    .then(res => res)
    .catch(err => err);
}

module.exports = {
  authenticate,
  register,
};
