/**
* @author Paulo Silva
* @description User model to define schema of User in mongodb
* @version 0.0.1
*/

const mongoose = require('mongoose');

/**
 * @example {username: 'Username', password: '1234', firstName: 'First Name', lastName: 'Last Name'}
 * @property schema.username
 * @type String
 * @property schema.password
 * @type String
 * @property schema.firstName
 * @type String
 * @property schema.lastName
 * @type String
 * @property schema.created_at
 * @default Date.now
 * @type Date
 */
const schema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});

// set schema return JSON
schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
