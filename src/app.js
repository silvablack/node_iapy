/**
 * @author Paulo Silva
 * @description Server
 * @returns app with instance of express
 * @version 1.0.0
 */
/**
 * @requires express lib to instance server
 */
const express = require('express');

/**
 * @constructor express()
 */
const app = express();

/**
 * @requires body parser to Response
 */
const bodyParser = require('body-parser');

/**
 * @requires cors to allow api connection
 */
const cors = require('cors');

/**
 * @requires include jsonwebtoken configuration
 */
const jwt = require('./helpers/jwt');

/**
 * @requires errorHandler for default response error
 */
const errorHandler = require('./helpers/error-handler');


app.use(bodyParser.urlencoded({
  extended: true,
}));

// define json response
app.use(bodyParser.json());

// enable connections
app.use(cors());

// secure api
app.use(jwt());

/**
 * @requires config routes to site index and company
 */
const index = require('./routes/index');

// versioning api. Version 1.0
const v1 = express.Router();

v1.use('/', index);

// api user routes
v1.use('/user', require('./routes/user'));

app.use('/', v1); // default api version 1.0

/**
 * @description Set routes config on server
 */
app.use('/v1', v1);

// default error response
app.use(errorHandler);


module.exports = app;
