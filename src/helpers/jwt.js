/**
 * @description Make authenticate with method JWT and define routes not apply
 */

const expressJwt = require('express-jwt');
const config = require('../config/config.json');
const userService = require('../users/user.service');

// check if the token user is revoked
async function isRevoked(req, payload, done) {
  const user = await userService.getById(payload.sub);
  if (!user) {
    return done(null, true);
  }
  return done();
}

// define function to jwt and define routes not apply
function jwt() {
  const { secret } = config;
  return expressJwt({ secret, isRevoked }).unless({
    path: [
      '/v1/user/auth',
      '/v1/user/register',
      '/v1',
      '/',
    ],
  });
}

module.exports = jwt;
