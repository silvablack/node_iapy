FROM node:8.12.0

RUN useradd --user-group --create-home --shell /bin/false app &&\
 npm install --global npm@6.4.1

ENV DIR=/home/

COPY package.json package-lock.json $DIR/app/
RUN chown -R app:app $DIR/*

USER app
WORKDIR $DIR/app
RUN npm install --no-cache --silent --progress=false

USER root

COPY . $DIR/app

RUN chown -R app:app $DIR/*

USER app

CMD ["nodemon", "src/index.js"]